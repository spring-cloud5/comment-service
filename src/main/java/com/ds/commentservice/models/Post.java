package com.ds.commentservice.models;


import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;



public class Post {
    private Long id;

    private String title;

    private String note;

    private Date date;

    private Long user_id;

    private Set<Comment> comments;


    public Post(String title, String note, Long user_id) {
        this.title = title;
        this.note = note;
        this.user_id = user_id;
    }

    public Post(Long post_id) {
        this.id=post_id;
    }

    public Post() {

    }


    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getNote() {
        return note;
    }

    public Date getDate() {
        return date;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }


}
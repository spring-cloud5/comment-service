package com.ds.commentservice.repository;

import com.ds.commentservice.models.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CommentRepository extends JpaRepository<Comment, String> {

    List<Comment> findAllByPostid(Long Postid);
//  @Query("SELECT co FROM Comment co WHERE co.post_id=?1")
//  List<Comment> findCommentByPost_id(Long post_id);

}

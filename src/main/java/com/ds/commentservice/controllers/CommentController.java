package com.ds.commentservice.controllers;

import com.ds.commentservice.dto.CommentDto;
import com.ds.commentservice.models.Comment;
import com.ds.commentservice.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CommentController {

    @Autowired
    CommentService commentService;

    @PostMapping("/createComment")
    Comment createComment(@RequestBody CommentDto comment){
        return this.commentService.createComment(comment);
    }

    @GetMapping("/getComments/{post_id}")
    List<Comment> getComments(@PathVariable Long post_id){
        return commentService.getComments(post_id);
    }
}

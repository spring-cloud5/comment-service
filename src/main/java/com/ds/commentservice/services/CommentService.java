package com.ds.commentservice.services;

import com.ds.commentservice.dto.CommentDto;
import com.ds.commentservice.models.Comment;
import com.ds.commentservice.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;

    public Comment createComment(CommentDto comment) {
        Comment newComment = new Comment(comment.getNote(),comment.getUser_id(),comment.getPost_id());
        return commentRepository.save(newComment);
    }

    public List<Comment> getComments(Long post_id) {
        return this.commentRepository.findAllByPostid(post_id);
    }
}
